
<x-layout>
    <x-setting :heading="'Edit Post: '.$post->title">

        <form action="/admin/posts/{{ $post->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')

            <x-form.input name="title" :value="$post->title" required></x-form.input>
            <x-form.input name="slug" :value="$post->slug" required></x-form.input>
            <x-form.textarea name="excerpt">{{ old('excerpt', $post->excerpt)}}</x-form.textarea>
            <x-form.textarea name="body">{{ old('body', $post->body)}}</x-form.textarea>

            <x-form.section>
                <x-form.label name="category_id"></x-form.label>

                <select name="category_id" id="category_id" required>
                    @php
                        $categories = \App\Models\Category::all();
                    @endphp

                    <option value="">Select category</option>
                    @foreach ($categories as $category)
                        <option 
                            value="{{ $category->id }}" 
                            {{ old('category_id', $post->category_id) == $category->id ? "selected" : "" }}>
                            {{ ucwords($category->name) }}</option>
                    @endforeach
                </select>

                <x-form.error name="category_id"></x-form.error>
            </x-form.section>

            <div class="flex mt-6">
                <div class="flex-1">
                    <x-form.input name="thumbnail" type="file"></x-form.input>
                </div>
                <img src="{{ asset('storage/'.$post->thumbnail) }}" alt="" class="rounded-xl ml-6" width="100">
            </div>


                

            <div class="flex justify-end mt-6">
                <x-submit-button>Update</x-submit-button>
            </div>
            
        </form>
    </x-setting>
</x-layout>