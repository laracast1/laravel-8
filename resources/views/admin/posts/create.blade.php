
<x-layout>
    <x-setting heading="Publish New Post">

        <form action="/admin/posts" method="POST" enctype="multipart/form-data">
            @csrf

            <x-form.input name="title" required></x-form.input>
            <x-form.input name="slug" required></x-form.input>
            <x-form.textarea name="excerpt"></x-form.textarea>
            <x-form.textarea name="body"></x-form.textarea>

            <x-form.section>
                <x-form.label name="category_id"></x-form.label>

                <select name="category_id" id="category_id" required>
                    @php
                        $categories = \App\Models\Category::all();
                    @endphp

                    <option value="">Select category</option>
                    @foreach ($categories as $category)
                        <option 
                            value="{{ $category->id }}" 
                            {{ old('category_id') == $category->id ? "selected" : "" }}>
                            {{ ucwords($category->name) }}</option>
                    @endforeach
                </select>

                <x-form.error name="category_id"></x-form.error>
            </x-form.section>

            <x-form.input name="thumbnail" type="file" required></x-form.input>
            

            <div class="flex justify-end mt-6">
                <x-submit-button>Publish</x-submit-button>
            </div>
            
        </form>
    </x-setting>
</x-layout>