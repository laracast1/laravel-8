                    
@props(['trigger'])

<!--all the alpine specific code is in this component.
Does not lick out into our views.This is nice-->

<div x-data="{ show: false }" @click.away="show = false" class="relative">
  <!-- Trigger -->
  <div @click="show = !show">
      {{ $trigger }}
  </div>            

  <!-- Dropdown links -->
  <div x-show="show" class="py-3 absolute bg-gray-100 w-full mt-1 rounded-xl z-50 overflow-auto max-h-52"  style="display: none;">
    {{ $slot }}                             
  </div>

</div>