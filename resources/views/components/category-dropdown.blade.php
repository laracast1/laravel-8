<div>
    <!-- If you do not have a consistent goal in life, you can not live it in a consistent way. - Marcus Aurelius -->
     <x-dropdown>
                        <x-slot name="trigger">
                            <button class="bg-transparent py-2 pl-3 pr-9 text-sm font-semibold w-full lg:w-32 text-left inline-flex">
                            
                                {{ isset($currentCategory) ? ucwords($currentCategory->name) : "Categories" }}

                                <x-down-arrow-svg class="absolute pointer-events-none" style="right: 12px;" ></x-down-arrow-svg>
                            
                            </button>
                        </x-slot>

                        <x-dropdown-item 
                            href="/?{{http_build_query(request()->except('category', 'page'))}}"
                            :active="request()->routeIs('home')">
                                All
                        </x-dropdown-item>

                        @foreach($categories as $category)
                            <x-dropdown-item 
                            href="/?category={{$category->slug}}&{{http_build_query(request()->except('category', 'page'))}}" 
                            :active="isset($currentCategory) && $currentCategory->is($category)"
                            >
                                {{ ucwords($category->name) }}
                                
                            </x-dropdown-item>
                        @endforeach 

                    </x-dropdown>
</div>