<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //User::truncate();
        //Category::truncate();
        // \App\Models\User::factory(10)->create();

        //give a specific name. use fake data for everything except the name
        $user = User::factory()->create([
            'name'  => 'John Doe'
        ]);

        Post::factory(20)->create();

        // Post::factory(20)->create([
        //     'user_id' => $user->id
        // ]);
    }
}
