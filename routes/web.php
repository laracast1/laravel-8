<?php

use App\Http\Controllers\AdminPostController;
use App\Models\Post;
use App\Models\User;
use App\Models\Category;
use App\Services\Newsletter;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\NewsletterController;
use App\Http\Controllers\SessionController;
use App\Http\Controllers\RegisterController;
use Illuminate\Validation\ValidationException;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [PostController::class, 'index'])->name('home');

//route model binding
Route::get('posts/{post:slug}', [PostController::class, 'show'])->name('post');

//add comment
Route::post('posts/{post:slug}/comments', [CommentController::class, 'store']);

Route::post('newsletter', NewsletterController::class);

Route::get('categories/{category:slug}', function(Category $category){
    return view('posts.index', [
        'posts' => $category->posts,
        'currentCategory' => $category,
    ]);
})->name('category');

Route::get('register', [RegisterController::class, 'create'])->middleware('guest');
Route::post('register', [RegisterController::class, 'store'])->middleware('guest');

Route::get('login', [SessionController::class, 'create'])->middleware('guest');
Route::post('login', [SessionController::class, 'store'])->middleware('guest');

//logout
Route::post('logout', [SessionController::class, 'destroy'])->middleware('auth');


//Admin

Route::middleware('can:admin')->group(function(){
    Route::resource('admin/posts', AdminPostController::class)->except('show');
});

//similar to
// Route::get('/admin/posts', [AdminPostController::class, 'index'])->middleware('admin');
// Route::get('admin/posts/create', [AdminPostController::class, 'create'])->middleware('admin');
// Route::post('/admin/posts', [AdminPostController::class, 'store'])->middleware('admin');
// Route::get('/admin/posts/{post}/edit', [AdminPostController::class, 'edit'])->middleware('admin');
// Route::patch('/admin/posts/{post}', [AdminPostController::class, 'update'])->middleware('admin');
// Route::delete('/admin/posts/{post}', [AdminPostController::class, 'destroy'])->middleware('admin');

// Route::get('authors/{author:username}', function(User $author){
//     return view('posts.index', [
//         'posts' => $author->posts,
//     ]);
// })->name('author');