<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function create(){
        return view('register.create');
    }

    public function store(){
        //var_dump(request()->all());

        $validated = request()->validate([
            'name' => 'required',
            'username' => 'required|min:3|unique:users',
            'email' => 'required|email|unique:users',
            "password" => 'required|min:7'
        ]);

        $validated['password'] = bcrypt(request()->password);

        $user = User::create($validated);

        //then log in user
        auth()->login($user);

        session()->flash('success-message', 'Your account has been created');

        return redirect('/');
    }
}
