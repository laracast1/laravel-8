<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AdminPostController extends Controller
{
    public function index()
    {
        return view('admin.posts.index', [
            'posts' => Post::latest()->paginate(50),
        ]);
    }

    /*show a form to create a post*/
    public function create()
    {
        return view('admin.posts.create');
    }

    public function store()
    {   
        $attributes = $this->validatePost();

        $attributes['user_id'] = auth()->id();
        // $attributes['slug'] = Str::slug(strtolower($attributes['title']), '-');

        //returns a path to where the file was
        $attributes['thumbnail'] = request()->file('thumbnail')->store('thumbnails');

        $post = Post::create($attributes);
        return redirect()->route('post', $post->slug)->with('success-message', 'Post published successfully');
    }

    public function edit(Post $post)
    {
        return view('admin.posts.edit', [
            'post' => $post,
        ]);
    }

    public function update(Post $post)
    {
        
        $attributes = $this->validatePost($post);

        //$attributes['user_id'] = auth()->id();
        // $attributes['slug'] = Str::slug(strtolower($attributes['title']), '-');

        //check if new file has been passed
        if(request()->file('thumbnail'))
        {
            //returns a path to where the file was
            $attributes['thumbnail'] = request()->file('thumbnail')->store('thumbnails');
        }
        

        $post->update($attributes);

        return redirect()->route('post', $post->slug)->with('success-message', 'Post updated successfully');
    }

    public function destroy(Post $post)
    {
        $post->delete();

        return redirect('/admin/posts')->with('success-message', 'Post deleted successfully');
    }

    protected function validatePost(?Post $post = null)
    {
        $post ??= new Post();

       return request()->validate([
            'title' => 'required|max:255',
            'slug' => ['required', Rule::unique('posts', 'slug')->ignore($post->id)],
            'excerpt' => 'required|max:255',
            'body' => 'required|max:255',
            'category_id' => ["required", Rule::exists('categories', 'id')],
            'thumbnail' => $post->exists ? ['image'] : ['required', 'image'],
        ]);
    }

}
