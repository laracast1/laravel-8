<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function create()
    {
        return view('sessions.create');
    }

    public function store(){
        //validate
        $validated = request()->validate([
            'email' => 'required|email|exists:users,email',
            'password' => 'required'
        ]);
        
        /*
        Attempt to log in the user based upon the provided
        credentails
        */
        if(auth()->attempt($validated))
        {
            //session regenerate
            session()->regenerate();
            
            return redirect('/')->with('success-message', 'Welcome back!');
        }
        
        //auth failed
        return back()
            ->withInput()
            ->withErrors(['email'=>'Your provided credentials could not be verified']);
    }
    
    public function destroy()
    {
        auth()->logout();

        return redirect('/')->with('success-message', 'Goodbye!');
    }
}
